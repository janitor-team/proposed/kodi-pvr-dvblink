# Kodi Media Center language file
# Addon Name: TVMosaic/DVBLink PVR Client
# Addon id: pvr.dvblink
# Addon Provider: DVBLogic
msgid ""
msgstr ""
"Project-Id-Version: KODI Main\n"
"Report-Msgid-Bugs-To: https://github.com/xbmc/xbmc/issues/\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"PO-Revision-Date: 2021-06-27 09:29+0000\n"
"Last-Translator: Christian Gade <gade@kodi.tv>\n"
"Language-Team: Ukrainian <https://kodi.weblate.cloud/projects/kodi-add-ons-pvr-clients/pvr-dvblink/uk_ua/>\n"
"Language: uk_ua\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.7\n"

msgctxt "Addon Summary"
msgid "PVR Plugin for TVMosaic/DVBLink"
msgstr "PVR плагін для TVMosaic/DVBLink"

msgctxt "Addon Description"
msgid "PVR Plugin for TVMosaic/DVBLink from DVBLogic (www.tv-mosaic.com); supporting streaming of Live TV & Recordings, EPG, Timers"
msgstr "PVR плагін для TVMosaic/DVBLink з DVBLogic (www.tv-mosaic.com); підтримує потоки Live TV, запис, програму передач, таймери."

msgctxt "Addon Disclaimer"
msgid "This is unstable software! The authors are in no way responsible for failed recordings, incorrect timers, wasted hours, or any other undesirable effects."
msgstr "Нестабільна програма! Автори не несуть відповідальності за зіпсовані записи, неправильні таймери, втрачений час та інші проблеми."

msgctxt "#30000"
msgid "General"
msgstr "Загальні"

msgctxt "#30001"
msgid "Server Address"
msgstr "Адреса сервера"

msgctxt "#30002"
msgid "Server Port"
msgstr "Порт сервера"

msgctxt "#30003"
msgid "Client name"
msgstr "Назва клієнта"

msgctxt "#30004"
msgid "Connection timeout (s)"
msgstr "Затримка зв’язку (сек)"

msgctxt "#30005"
msgid "Username"
msgstr "Ім'я користувача"

msgctxt "#30006"
msgid "Password"
msgstr "Пароль"

msgctxt "#30007"
msgid "Playback failed, please ensure you are using TV MOSAIC PLUS"
msgstr ""

msgctxt "#30100"
msgid "Stream"
msgstr "Потік"

msgctxt "#30102"
msgid "Enable transcoding"
msgstr "Увімкнути конвертування"

msgctxt "#30103"
msgid "Height"
msgstr "Висота"

msgctxt "#30104"
msgid "Width"
msgstr "Ширина"

msgctxt "#30105"
msgid "Bitrate"
msgstr "Бітрейт"

msgctxt "#30106"
msgid "Audio track"
msgstr "Звукова доріжка"

msgctxt "#30107"
msgid "HTTP"
msgstr "HTTP"

msgctxt "#30108"
msgid "RTP"
msgstr "RTP"

msgctxt "#30109"
msgid "HLS"
msgstr "HLS"

msgctxt "#30110"
msgid "ASF"
msgstr "ASF"

msgctxt "#30111"
msgid "Enable Timeshift"
msgstr "Увімкнути зсув у часі"

msgctxt "#30112"
msgid "Timeshift buffer path"
msgstr "Шлях до буферу зсуву у часі"

# empty strings from id 30113 to 30119
msgctxt "#30120"
msgid "1 min"
msgstr ""

msgctxt "#30121"
msgid "2 min"
msgstr ""

msgctxt "#30122"
msgid "3 min"
msgstr ""

msgctxt "#30123"
msgid "4 min"
msgstr ""

msgctxt "#30124"
msgid "5 min"
msgstr ""

msgctxt "#30125"
msgid "6 min"
msgstr ""

msgctxt "#30126"
msgid "7 min"
msgstr ""

msgctxt "#30127"
msgid "8 min"
msgstr ""

msgctxt "#30128"
msgid "9 min"
msgstr ""

msgctxt "#30129"
msgid "10 min"
msgstr ""

msgctxt "#30130"
msgid "20 min"
msgstr ""

msgctxt "#30131"
msgid "30 min"
msgstr ""

msgctxt "#30132"
msgid "40 min"
msgstr ""

msgctxt "#30133"
msgid "50 min"
msgstr ""

msgctxt "#30134"
msgid "60 min"
msgstr ""

msgctxt "#30200"
msgid "Advanced"
msgstr "Додатково"

msgctxt "#30201"
msgid "Use channel handle instead of client id"
msgstr "Використовувати мітку каналу замість ID клієнта"

msgctxt "#30202"
msgid "Show information messages"
msgstr "Показати інформаційне повідомлення"

msgctxt "#30203"
msgid "Combine title and episode name for recordings"
msgstr "Комбінувати назву та ім'я епізоду у записах"

msgctxt "#30204"
msgid "Group Recordings by Series"
msgstr "Групувати записи за серіалами"

msgctxt "#32001"
msgid "Connected to DVBLink Server '%s'"
msgstr "З'єднання з сервером DVBLink '%s'"

msgctxt "#32002"
msgid "Found '%d' channels"
msgstr "Знайдено каналів: '%d'"

msgctxt "#32003"
msgid "Could not connect to DVBLink Server '%s' (Error code : %d)"
msgstr "Не вдалося з'єднатися з сервером DVBLink '%s' (Код помилки : %d)"

msgctxt "#32004"
msgid "Could not get recordings (Error code : %d)"
msgstr "Не вдалося отримати записи (Код помилки: %d)"

msgctxt "#32006"
msgid "Could not get timers(Error code : %d)"
msgstr "Не вдалося отримати таймери (Код помилки: %d)"

msgctxt "#32007"
msgid "Found %d EPG timers"
msgstr "Знайдено таймерів EPG: %d"

msgctxt "#32008"
msgid "Found %d manual timers"
msgstr "Знайдено ручних таймерів: %d"

msgctxt "#32009"
msgid "Found %d recordings"
msgstr "Знайдено записів: %d"

msgctxt "#32010"
msgid "Could not get stream for channel %s (Error code : %d)"
msgstr "Не вдалося отримати потік каналу %s (Код помилки: %d)"

msgctxt "#32011"
msgid "Series recording"
msgstr "Запис серіалів"

msgctxt "#32012"
msgid "Record this episode only"
msgstr "Записати тільки цю серію"

msgctxt "#32013"
msgid "Record all episodes"
msgstr "Записати усі серії"

msgctxt "#32014"
msgid "Delete timer"
msgstr "Видалити таймер"

msgctxt "#32015"
msgid "Delete this timer only"
msgstr "Видалити лише цей таймер"

msgctxt "#32016"
msgid "Delete the entire series"
msgstr "Видалити весь серіал"

msgctxt "#32017"
msgid "Margin before (minutes)"
msgstr "Маржа перед (хв)"

msgctxt "#32018"
msgid "Margin after (minutes)"
msgstr "Маржа після (хв)"

msgctxt "#32019"
msgid "New episodes only"
msgstr "Тільки нові серії"

msgctxt "#32020"
msgid "Broadcast anytime"
msgstr "Трансляція в будь-який час"

msgctxt "#32021"
msgid "Number of episodes to keep"
msgstr "Кількість серій для збереження"

msgctxt "#32022"
msgid "Default"
msgstr "Як усталено"

msgctxt "#32023"
msgid "Keep all"
msgstr "Залишити всі"

msgctxt "#32024"
msgid "Playback failed. Server does not support transcoding"
msgstr "Не вдалося відтворити. Сервер не підтримує перекодування"

msgctxt "#32025"
msgid "Do not group single recordings"
msgstr "Не групувати окремі записи"

msgctxt "#32026"
msgid "Keep all recordings"
msgstr ""

msgctxt "#32027"
msgid "Keep 1 recording"
msgstr ""

msgctxt "#32028"
msgid "Keep 2 recordings"
msgstr ""

msgctxt "#32029"
msgid "Keep 3 recordings"
msgstr ""

msgctxt "#32030"
msgid "Keep 4 recordings"
msgstr ""

msgctxt "#32031"
msgid "Keep 5 recordings"
msgstr ""

msgctxt "#32032"
msgid "Keep 6 recordings"
msgstr ""

msgctxt "#32033"
msgid "Keep 7 recordings"
msgstr ""

msgctxt "#32034"
msgid "Keep 10 recordings"
msgstr ""

msgctxt "#32035"
msgid "Record all episodes"
msgstr "Записати усі серії"

msgctxt "#32036"
msgid "Record only new episodes"
msgstr "Записувати лише нові серії"

msgctxt "#32037"
msgid "Single shot manual timer"
msgstr ""

msgctxt "#32038"
msgid "Single shot EPG-based timer"
msgstr ""

msgctxt "#32039"
msgid "Generated by repeating timer"
msgstr ""

msgctxt "#32040"
msgid "Generated by record series timer"
msgstr ""

msgctxt "#32041"
msgid "Generated by repeating keyword timer"
msgstr ""

msgctxt "#32042"
msgid "Repeating manual timer"
msgstr ""

msgctxt "#32043"
msgid "Record series timer"
msgstr ""

msgctxt "#32044"
msgid "Repeating keyword-based timer"
msgstr ""
